import React, { useContext } from 'react'
import './Placeorder.css'
import { Storecontext } from '../../context/Storecontext'

const Placeorder = () => {

  const {getTotalCartAmount} = useContext(Storecontext)

  return (
    <form className='place-order'>
      <div className='place-order-left'>
        <p className='title'>
          Delivery Information
        </p>
        <div className='multi-fields'>
          <input type="text" placeholder='First Name' />
          <input type="text" placeholder='Last Name' />

        </div>
        <input type="email" placeholder='Email-Address' />
        <input type="text" placeholder='Street' />
        <div className='multi-fields'>
        <input type="text" placeholder='City' />
          <input type="text" placeholder='State' />

        </div>
        <div className='multi-fields'>
        <input type="text" placeholder='Zipcode' />
          <input type="text" placeholder='Country' />

        </div>
        <div>
          <input type="text" placeholder='Phone' />
        </div>

      </div>
      <div className="place-order-right">
      <div className="cart-total">
          <h2>Cart Totals</h2>
          <div>
            <div className="cart-details">
              <p>Subtotal</p>
              <p>${getTotalCartAmount()}</p>
            </div>
            <hr />
            <div className="cart-details">
              <p>Delivery fees</p>
              <p>${getTotalCartAmount()===0?0:2}</p>
            </div>
            <hr />
            <div className="cart-details">
              <b>Total</b>
              <b>${getTotalCartAmount()===0?0:getTotalCartAmount()+2}</b>
            </div>
            <button>PROCEED TO PAYMENT</button>
          </div>
        </div>

      </div>
    

    </form>
  )
}

export default Placeorder
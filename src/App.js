import logo from './logo.svg';
import './App.css';
import Navbar from './components/Navbar/Navbar';
import { Route, Routes } from 'react-router-dom';
import Home from './Pages/Home/Home';
import Cart from './Pages/Cart/Cart';
import Placeorder from './Pages/Placeorder/Placeorder';
import Footer from './components/Footer/Footer';
import { useState } from 'react';
import Loginpopup from './components/Loginpopup/Loginpopup';

function App() {

  const [showLogin,setShowLogin] = useState(false)

  
  return (
    <>
    {showLogin?<Loginpopup setShowLogin={setShowLogin}/>:<></>}
     <div>
     <Navbar setShowLogin={setShowLogin} />
     <Routes>
      <Route path='/' element={<Home/>}/>
      <Route path='/Cart' element={<Cart/>}/>
      <Route path='/Order' element={<Placeorder/>}/>
     </Routes>
    </div>
    <Footer />
    </>
   
  );
}

export default App;

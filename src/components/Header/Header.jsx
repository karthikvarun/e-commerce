import React, { useState } from 'react'
import './Header.css';
import backgroundImage from './header_img.png';

const Header = () => {
  const [menu,setMenu] = useState("menu");
  return (
    <div className='header'>
      <img src={backgroundImage} alt="Background" className="background-img" />
      <div className='header-contents'>
        <h2>Order your favourite food here</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem tempore tempora quod vel debitis iusto?</p>
        <a href='#Explore-menu' onClick={()=>setMenu("menu")}><button>View menu</button></a>
        
      </div>
    </div>
  );
}

export default Header;

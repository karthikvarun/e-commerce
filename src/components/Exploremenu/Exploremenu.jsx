import React from 'react'
import './Exploremenu.css'
import { menu_list } from '../../assets/assets'
const Exploremenu = ({category , setCategory}) => {
  return (
    <div className='Explore-menu' id='Explore-menu'>
        <h1>Explore our menu</h1>
        <p className='Explore-menu-text'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quas quidem adipisci autem doloribus tenetur perferendis quis odit itaque cupiditate totam, consequuntur nam voluptate at rerum sit vitae, repellat, atque nisi.</p>
        <div className="explore-menu-list">
            {menu_list.map((item, index)=>{
                return(
                    <div onClick={()=>setCategory(prev=>prev===item.menu_name?"all":item.menu_name)} key={index}  className="explore-menu-list-item">
                        <img className={category===item.menu_name?"active":""} src={item.menu_image} alt="" />
                        <p>{item.menu_name}</p>

                    </div>
                )
            })}
        </div>
        <hr />

    </div>
  )
}

export default Exploremenu